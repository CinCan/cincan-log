import pathlib

from cincan_log.log_index import CommandLogIndex, CommandLog
from cincan_log.log_search import CommandStringSearch


def test_string_search():
    index = CommandLogIndex(pathlib.Path('_none'), dummy_log=True)
    index.array = [
        CommandLog(command=['first_command']),
        CommandLog(command=['what a command']),
        CommandLog(command=['whatever']),
    ]
    s = CommandStringSearch('what*')
    result = s.search(index)
    assert [r.__str__() for r in result] == [
        '"what a command"',
        'whatever'
    ]

