from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='cincan_log',
    version='0.0.1',
    author="Rauli Kaksonen",
    author_email="rauli.kaksonen@gmail.com",
    description='Cincan log manipulation',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/cincan/cincan-log",
    packages=['cincan_log'],
    install_requires=[],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    # entry_points={
    #     'console_scripts': ['cincan-log=cincan_log.start:main'],
    # },
    python_requires='>=3.6',
)
