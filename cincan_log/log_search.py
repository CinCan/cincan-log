import pathlib
from typing import List

from cincan_log.log_index import CommandLogIndex, quote_args


class LogSearch:
    def search(self, index: CommandLogIndex) -> List:
        return []


class CommandStringSearch(LogSearch):
    def __init__(self, pattern: str):
        self.pattern = pattern

    def search(self, index: CommandLogIndex) -> List[str]:
        r = []
        for e in index.list_entries():
            cmd = " ".join(e.command)
            m = pathlib.PurePath(cmd).match(self.pattern)
            if m:
                r.append(" ".join(quote_args(e.command)))
        return r
