import argparse
import logging
import pathlib
import sys

from cincan_log.command_inspector import CommandInspector
from cincan_log.log_index import CommandLogIndex


def main():
    """Parse command line and run the tool"""
    m_parser = argparse.ArgumentParser()
    m_parser.add_argument("-l", "--log", dest="log_level", choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                          help="Set the logging level", default=None)
    m_parser.add_argument('-q', '--quiet', action='store_true', help='Be quite quiet')
    subparsers = m_parser.add_subparsers(dest='sub_command')

    fanin_parser = subparsers.add_parser('fanin', help='Show fan-in to the given file')
    fanin_parser.add_argument('file', help="File to analyze")
    fanin_parser.add_argument('-d', '--max-depth', default=1, help='Maximum tree depth')

    fanout_parser = subparsers.add_parser('fanout', help='Show fan-out from the given file')
    fanout_parser.add_argument('file', help="File to analyze")
    fanout_parser.add_argument('-d', '--max-depth', default=1, help='Maximum tree depth')

    args = m_parser.parse_args(args=sys.argv[1:])

    log_level = args.log_level if args.log_level else ('WARNING' if args.quiet else 'INFO')
    if log_level not in {'DEBUG'}:
        sys.tracebacklimit = 0  # avoid track traces unless debugging
    logging.basicConfig(format='%(name)s: %(message)s', level=getattr(logging, log_level))

    sub_command = args.sub_command
    if not sub_command or sub_command == 'help':
        m_parser.print_help()
        sys.exit(1)
    elif sub_command in {'fanin', 'fanout'}:
        inspector = CommandInspector(CommandLogIndex(), pathlib.Path().resolve())
        depth = int(args.max_depth)
        if sub_command == 'fanout':
            res = inspector.fanout(pathlib.Path(args.file).resolve(), depth)
        else:
            res = inspector.fanin(pathlib.Path(args.file).resolve(), depth)
        print(res)
    else:
        sys.exit(f"Unexpected sub command '{sub_command}")
