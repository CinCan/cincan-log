import json
import pathlib
import uuid
from datetime import datetime
from typing import Optional, Any, Dict

JSON_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'


class CommandLogBase:
    """Command log reader/writer base class"""
    def __init__(self, log_directory: Optional[pathlib.Path] = None, dummy_log: bool = False):
        self.uid_file = pathlib.Path.home() / '.cincan/uid.txt'
        self.file_name_format = '%Y-%m-%d-%H-%M-%S-%f'

        # check if .cincan contains uid string, don't create new folder
        if dummy_log:
            self.directory_name = "NONE"
        elif self.uid_file.is_file():
            with self.uid_file.open("r") as f:
                self.directory_name = f.read()
        else:
            # create uuid.object and folder and such
            self.directory_name = str(uuid.uuid1())
            with self.uid_file.open("w") as uid_file:
                uid_file.write(self.directory_name)

        self.log_directory = log_directory or pathlib.Path.home() / '.cincan' / 'shared' / self.directory_name / 'logs'
        if not dummy_log:
            self.log_directory.mkdir(parents=True, exist_ok=True)

    def write(self, json_s: Dict[str, Any]):
        log_file = self.__log_file()
        while log_file.exists():
            log_file = self.__log_file()
        with log_file.open("w") as f:
            json.dump(json_s, f)

    def __log_file(self) -> pathlib.Path:
        return self.log_directory / datetime.now().strftime(self.file_name_format)
