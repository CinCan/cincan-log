import json
import pathlib
import string
from typing import Dict, Any, Optional, List, Iterable
from datetime import datetime

from cincan_log.command_log import JSON_TIME_FORMAT, CommandLogBase


def quote_args(args: Iterable[str]) -> List[str]:
    """Quote the arguments which contain whitespaces"""
    r = []
    for arg in args:
        if any(map(lambda c: c in string.whitespace, arg)):
            r.append(f'"{arg}"')
        else:
            r.append(arg)
    return r


class FileLog:
    """Command log entry for a file"""
    def __init__(self, path: pathlib.Path, digest: str, timestamp: Optional[datetime] = None):
        self.path = path
        self.digest = digest
        self.timestamp = timestamp

    def to_json(self) -> Dict[str, Any]:
        js = {
            'path': self.path.as_posix()
        }
        if self.digest:
            js['sha256'] = self.digest
        if self.timestamp:
            js['timestamp'] = self.timestamp.strftime(JSON_TIME_FORMAT)
        return js

    @classmethod
    def from_json(cls, js: Dict[str, Any]) -> 'FileLog':
        log = FileLog(pathlib.Path(js.get('name') or js.get('path')), js.get('sha256', ''))
        if 'timestamp' in js:
            log.timestamp = datetime.strptime(js['timestamp'], JSON_TIME_FORMAT)
        return log

    def __repr__(self) -> str:
        return json.dumps(self.to_json(), indent=4)


class CommandLog:
    """Command log entry"""
    def __init__(self, command: List[str], timestamp: datetime = datetime.now()):
        self.command = command
        self.timestamp = timestamp
        self.exit_code = 0
        self.stdin: Optional[bytes] = None
        self.stdout: Optional[bytes] = None
        self.stderr: Optional[bytes] = None
        self.in_files: List[FileLog] = []
        self.out_files: List[FileLog] = []

    def command_string(self) -> str:
        return " ".join(quote_args(self.command))

    def to_json(self) -> Dict[str, Any]:
        js = {
            'command': self.command,
            'timestamp': self.timestamp.strftime(JSON_TIME_FORMAT),
            'exit_code': self.exit_code,
        }
        if len(self.in_files) > 0:
            js['input'] = [f.to_json() for f in self.in_files]
        if len(self.out_files) > 0:
            js['output'] = [f.to_json() for f in self.out_files]
        return js

    @classmethod
    def from_json(cls, js: Dict[str, Any]) -> 'CommandLog':
        t = datetime.strptime(js.get('timestamp') or js.get('end_time'), JSON_TIME_FORMAT)
        log = CommandLog(js['command'], t)

        if 'input' in js:
            log.in_files = [FileLog.from_json(fs) for fs in js['input']]
        if 'output' in js:
            log.out_files = [FileLog.from_json(fs) for fs in js['output']]
        return log

    def __repr__(self) -> str:
        return json.dumps(self.to_json(), indent=4)


class CommandLogIndex(CommandLogBase):
    """Command log index for reading command log"""
    def __init__(self, log_directory: Optional[pathlib.Path] = None, dummy_log: bool = False):
        super().__init__(log_directory, dummy_log)
        log_root = self.log_directory.parent or self.log_directory  # read log from all users
        if not dummy_log:
            self.array = self.__read_log(log_root)
        else:
            self.array = []

    def list_entries(self, reverse: bool = False) -> Iterable[CommandLog]:
        return sorted(self.array, key=lambda e: e.timestamp, reverse=reverse)

    def __read_log(self, directory: pathlib.Path) -> List[CommandLog]:
        log_l = []
        for file in self.log_directory.iterdir():
            if file.is_dir():
                # recursively go to sub d
                log_l.extend(self.__read_log(file))
            else:
                with file.open('r') as f:
                    js = json.load(f)
                    log_l.append(CommandLog.from_json(js))
        return log_l
